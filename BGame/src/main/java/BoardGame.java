
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dinochat
 */
public class BoardGame {
    
    protected String title;
    protected double price;
    protected String players;
    protected int in_stock;
    protected int id;

    public BoardGame(String title, double price, String players, int in_stock, int id) {
        this.title = title;
        this.price = price;
        this.players = players;
        this.in_stock = in_stock;
        this.id = id;
    }
    
    public static List<BoardGame> all() throws SQLException, ClassNotFoundException{
       Connection connection = Myconnexion.getconnection(); //se connecter à la base de données
       Statement statement = Myconnexion.createStatement(connection);
       String query = "SELECT * FROM boardgame"; //récupère la requête pour la base de données
       ResultSet resultSet = Myconnexion.executeQuery(statement, query);
       List<BoardGame> lignes; //creation de la liste
       lignes = new ArrayList<>(); 
       
       while(resultSet.next()){
            lignes.add(BoardGame.createFromResultSec(resultSet));
        }
     
       return lignes;
    }
    
    public Object[] asVector(){
        return new Object[] { this.id, this.title, this.players, this.price, this.in_stock };
    }
    
    public static BoardGame createFromResultSec(ResultSet resultSet) throws SQLException{
        return new BoardGame(
                resultSet.getString("title"), 
                resultSet.getDouble("price"),
                resultSet.getString("players"), 
                resultSet.getInt("in_stock"),
                resultSet.getInt("id") 
        );
    }
    
    public void delete() throws SQLException, ClassNotFoundException{
        Connection con = Myconnexion.getconnection();
        String query = ("DELETE FROM BoardGame WHERE id = ?;"); //requête SQL pour supprimer
        PreparedStatement pst = con.prepareStatement(query);
        pst.setInt(1, this.id);
        pst.executeUpdate();
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public void setIn_stock(int in_stock) {
        this.in_stock = in_stock;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public String getPlayers() {
        return players;
    }

    public int getIn_stock() {
        return in_stock;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Class_BoardGames{" + "title=" + title + ", price=" + price + ", players=" + players + ", in_stock=" + in_stock + ", id=" + id + '}';
    }
    
}
