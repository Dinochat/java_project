
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dinochat
 */
public class Myconnexion {
    
        protected static String dbPath;

        public static void setDbPath(String path) {
            Myconnexion.dbPath = String.format("jdbc:sqlite:%s", path);
        }
        
         public static void ChoixBDD(){
            JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

		int returnValue = jfc.showOpenDialog(null);

		if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = jfc.getSelectedFile();
                    System.out.println(selectedFile.getAbsolutePath());
                    Myconnexion.setDbPath(selectedFile.getAbsolutePath());
		}
        }
    
        public static Connection getconnection() throws SQLException, ClassNotFoundException{
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection(Myconnexion.dbPath);

            return connection;
        } 

        public static Statement createStatement(Connection connection) throws SQLException {
            return connection.createStatement();
        }
        
        public static PreparedStatement createPreparedStatement(Connection connection, String query) throws SQLException {
            return connection.prepareStatement(query);
        }

        public static ResultSet executeQuery(Statement statement, String query) throws SQLException {
            return statement.executeQuery(query);
        }
        
        public static ResultSet executePreparedQuery(PreparedStatement preparedStatement) throws SQLException{
            return preparedStatement.executeQuery();
        }
        
       
}

/*
this.refrechTable1();


public void refrechTable1() throws SQLException, ClassNotFoundException{
        DefaultTableModel tableModel = (DefaultTableModel) Table1.getModel();
        
        List<Class_BoardGames> boardgames = Class_BoardGames.all();
        
        tableModel.insertRow(0, boardgames.get(0).asVector());
    }
*/
